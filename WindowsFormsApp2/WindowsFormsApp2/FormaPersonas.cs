﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class FormaPersonas : Form
    {

        // Constructor
        public FormaPersonas()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            txtNombre.Text = "Boton aceptar";

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            txtNombre.Text = "Boton Cancelar";
        }

        private void FormaPersonas_Load(object sender, EventArgs e)
        {
            txtNombre.Text = "Teclee el nombre de la persona";
            txtApellido.Text = "Teclee el apellido de la persona";
            txtDireccion.Text = "Teclee la direccion completa";
            txtEmail.Text = "<mail>@<dominio>";
        }

    }
}
