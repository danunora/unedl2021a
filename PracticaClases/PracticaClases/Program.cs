﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaClases
{
    class Empleado
    {
        String nombre;
        String apellido;
        int noempleado;

        public Empleado(string nombre, string apellido, int noempleado)
        {
            Console.WriteLine("Ejecutando el 1er constructor");
            this.nombre = nombre;
            this.apellido = apellido;
            this.noempleado = noempleado;
        }

        public Empleado(string nombre, string apellido)
        {
            Console.WriteLine("Ejecutando el 2do constructor");
            this.nombre = nombre;
            this.apellido = apellido;
            this.noempleado = 0;
        }

        public Empleado()
        {
            Console.WriteLine("Ejecutando el 3er constructor");
            this.nombre = "";
            this.apellido = "";
            this.noempleado = 0;
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public int Noempleado { get => noempleado; set => noempleado = value; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("instanciando al empleado");
            Empleado empleado1 = new Empleado("Daniel", "Nuno", 1);
            Console.WriteLine("empleado1: " + empleado1.Nombre);
            Console.WriteLine("empleado1: " + empleado1.Apellido);
            Console.WriteLine("empleado1: " + empleado1.Noempleado);
            Console.WriteLine("Cambiando un dato del empleado1");
            empleado1.Nombre = "Alejandro";
            Console.WriteLine("empleado1: " + empleado1.Nombre);

            Console.WriteLine("Constructores");
            Empleado empleado2 = new Empleado("Alejandro", "Ramirez");
            Console.WriteLine("empleado2: " + empleado2.Nombre);
            Console.WriteLine("empleado2: " + empleado2.Apellido);
            Console.WriteLine("empleado2: " + empleado2.Noempleado);

            Empleado empleado3 = new Empleado();
            Console.Clear();
            Console.WriteLine("Ingrese los datos del empleado");
            Console.WriteLine("Nombre: ");
            empleado3.Nombre = Console.ReadLine();
            Console.WriteLine("Apellido: ");
            empleado3.Apellido = Console.ReadLine();
            Console.WriteLine("No Empleado: ");
            empleado3.Noempleado = Convert.ToInt32(Console.ReadLine());
       
            Console.WriteLine("empleado3: " + empleado3.Nombre);
            Console.WriteLine("empleado3: " + empleado3.Apellido);
            Console.WriteLine("empleado3: " + empleado3.Noempleado);

        }
    }
}
