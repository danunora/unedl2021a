﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblResultadoOperacion.Text = "0";
            txtValor1.Text = "0";
            txtValor2.Text = "0";
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            double a = Convert.ToDouble(txtValor1.Text);
            double b = Convert.ToDouble(txtValor2.Text);
            double c = a + b;
            lblResultadoOperacion.Text = c.ToString() ;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            lblResultadoOperacion.Text = "0";
            txtValor1.Text = "0";
            txtValor2.Text = "0";
        }
    }
}
