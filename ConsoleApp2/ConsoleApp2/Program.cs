﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Empleado
    {
        private String nombrex;
        private String apellidox;
        private int edadx;

        public Empleado(String nombre, String apellido, int edad)
        {
            this.nombrex = nombre;
            this.apellidox = apellido;
            this.edadx = edad;
        }

        public String PrintEmpleado()
        {
            return "Nombre: " + nombrex + " Apellido: " + apellidox + " Edad: " + edadx;
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            Empleado empleado1 = new Empleado("Daniel", "Nuno",48);

            Console.WriteLine(empleado1.PrintEmpleado());

        }
    }
}
