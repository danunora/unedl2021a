﻿namespace _2doParcial
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCargarArchivo = new System.Windows.Forms.Button();
            this.lblCargarArchivo = new System.Windows.Forms.Label();
            this.lblLeerArchivo = new System.Windows.Forms.Label();
            this.btnLeerArchivo = new System.Windows.Forms.Button();
            this.lblProcesarArchivo = new System.Windows.Forms.Label();
            this.btnProcesarArchivo = new System.Windows.Forms.Button();
            this.btnDesplegarParametros = new System.Windows.Forms.Button();
            this.txtDesplegarParametros = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // btnCargarArchivo
            // 
            this.btnCargarArchivo.Location = new System.Drawing.Point(25, 45);
            this.btnCargarArchivo.Name = "btnCargarArchivo";
            this.btnCargarArchivo.Size = new System.Drawing.Size(141, 23);
            this.btnCargarArchivo.TabIndex = 0;
            this.btnCargarArchivo.Text = "Cargar archivo...";
            this.btnCargarArchivo.UseVisualStyleBackColor = true;
            this.btnCargarArchivo.Click += new System.EventHandler(this.btnCargarArchivo_Click);
            // 
            // lblCargarArchivo
            // 
            this.lblCargarArchivo.AutoSize = true;
            this.lblCargarArchivo.Location = new System.Drawing.Point(25, 87);
            this.lblCargarArchivo.Name = "lblCargarArchivo";
            this.lblCargarArchivo.Size = new System.Drawing.Size(35, 13);
            this.lblCargarArchivo.TabIndex = 1;
            this.lblCargarArchivo.Text = "label1";
            // 
            // lblLeerArchivo
            // 
            this.lblLeerArchivo.AutoSize = true;
            this.lblLeerArchivo.Location = new System.Drawing.Point(25, 169);
            this.lblLeerArchivo.Name = "lblLeerArchivo";
            this.lblLeerArchivo.Size = new System.Drawing.Size(35, 13);
            this.lblLeerArchivo.TabIndex = 3;
            this.lblLeerArchivo.Text = "label1";
            // 
            // btnLeerArchivo
            // 
            this.btnLeerArchivo.Location = new System.Drawing.Point(25, 127);
            this.btnLeerArchivo.Name = "btnLeerArchivo";
            this.btnLeerArchivo.Size = new System.Drawing.Size(141, 23);
            this.btnLeerArchivo.TabIndex = 2;
            this.btnLeerArchivo.Text = "Leer archivo...";
            this.btnLeerArchivo.UseVisualStyleBackColor = true;
            this.btnLeerArchivo.Click += new System.EventHandler(this.btnLeerArchivo_Click);
            // 
            // lblProcesarArchivo
            // 
            this.lblProcesarArchivo.AutoSize = true;
            this.lblProcesarArchivo.Location = new System.Drawing.Point(25, 263);
            this.lblProcesarArchivo.Name = "lblProcesarArchivo";
            this.lblProcesarArchivo.Size = new System.Drawing.Size(35, 13);
            this.lblProcesarArchivo.TabIndex = 5;
            this.lblProcesarArchivo.Text = "label1";
            // 
            // btnProcesarArchivo
            // 
            this.btnProcesarArchivo.Location = new System.Drawing.Point(25, 221);
            this.btnProcesarArchivo.Name = "btnProcesarArchivo";
            this.btnProcesarArchivo.Size = new System.Drawing.Size(141, 23);
            this.btnProcesarArchivo.TabIndex = 4;
            this.btnProcesarArchivo.Text = "Procesar archivo...";
            this.btnProcesarArchivo.UseVisualStyleBackColor = true;
            this.btnProcesarArchivo.Click += new System.EventHandler(this.btnProcesarArchivo_Click);
            // 
            // btnDesplegarParametros
            // 
            this.btnDesplegarParametros.Location = new System.Drawing.Point(25, 314);
            this.btnDesplegarParametros.Name = "btnDesplegarParametros";
            this.btnDesplegarParametros.Size = new System.Drawing.Size(141, 23);
            this.btnDesplegarParametros.TabIndex = 6;
            this.btnDesplegarParametros.Text = "Desplegar parametros";
            this.btnDesplegarParametros.UseVisualStyleBackColor = true;
            this.btnDesplegarParametros.Click += new System.EventHandler(this.btnDesplegarParametros_Click);
            // 
            // txtDesplegarParametros
            // 
            this.txtDesplegarParametros.Location = new System.Drawing.Point(220, 127);
            this.txtDesplegarParametros.Multiline = true;
            this.txtDesplegarParametros.Name = "txtDesplegarParametros";
            this.txtDesplegarParametros.Size = new System.Drawing.Size(341, 210);
            this.txtDesplegarParametros.TabIndex = 7;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 370);
            this.Controls.Add(this.txtDesplegarParametros);
            this.Controls.Add(this.btnDesplegarParametros);
            this.Controls.Add(this.lblProcesarArchivo);
            this.Controls.Add(this.btnProcesarArchivo);
            this.Controls.Add(this.lblLeerArchivo);
            this.Controls.Add(this.btnLeerArchivo);
            this.Controls.Add(this.lblCargarArchivo);
            this.Controls.Add(this.btnCargarArchivo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCargarArchivo;
        private System.Windows.Forms.Label lblCargarArchivo;
        private System.Windows.Forms.Label lblLeerArchivo;
        private System.Windows.Forms.Button btnLeerArchivo;
        private System.Windows.Forms.Label lblProcesarArchivo;
        private System.Windows.Forms.Button btnProcesarArchivo;
        private System.Windows.Forms.Button btnDesplegarParametros;
        private System.Windows.Forms.TextBox txtDesplegarParametros;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

