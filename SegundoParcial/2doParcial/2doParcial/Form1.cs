﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace _2doParcial
{
    public partial class Form1 : Form
    {
        public String FileName = "";
        public List<string> lineas = new List<string>();
        Dictionary<string, string> parametros = new Dictionary<string, string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnCargarArchivo_Click(object sender, EventArgs e)
        {
            openFileDialog1 = new OpenFileDialog()
            {
                FileName = "Select a text file",
                Filter = "Text files (*.txt)|*.txt",
                Title = "Open text file"
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileName = openFileDialog1.FileName;
                lblCargarArchivo.Text = FileName;
            }

        }

        private void btnLeerArchivo_Click(object sender, EventArgs e)
        {
            if (!FileName.Trim().Equals(""))
            {
                try
                {
                    lineas = System.IO.File.ReadAllLines(lblCargarArchivo.Text).ToList();
                }
                catch(IOException ioe)
                {
                    Console.WriteLine(ioe.Message);
                }
                lblLeerArchivo.Text = lineas.Count().ToString() + " lineas fueron procesadas";
            }
        }

        private void btnProcesarArchivo_Click(object sender, EventArgs e)
        {
            parametros.Clear();
            if (lineas.Count() != 0)
            {
                foreach(String linea in lineas)
                {
                    if (linea.Length != 0)
                    {
                        String primerCaracter = linea.Substring(0, 1);
                        if (!primerCaracter.Equals("#") &&
                            (!primerCaracter.Equals(" ")))
                        {
                            String llave = "", valor = "";
                            int posicion = linea.IndexOf("=");
                            int strsize = linea.Length;
                            llave = linea.Substring(0, posicion);
                            valor = linea.Substring(posicion + 1, strsize - posicion - 1);
                            parametros.Add(llave, valor);
                        }
                    }
                }
                lblProcesarArchivo.Text = parametros.Count().ToString();
            }
        }

        private void btnDesplegarParametros_Click(object sender, EventArgs e)
        {
            if (parametros.Count() != 0)
            {
                foreach (KeyValuePair<string, string> kvp in parametros)
                {
                    txtDesplegarParametros.AppendText(
                        kvp.Key + "=" + kvp.Value + "\n");
                }
            }
        }
    }
}
