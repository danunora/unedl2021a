﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal
{
    class Empleado
    {
        String nombre;
        String apellido;
        int noempleado;

        public Empleado(string nombre, string apellido, int noempleado)
        {
            Console.WriteLine("Ejecutando el 1er constructor");
            this.nombre = nombre;
            this.apellido = apellido;
            this.noempleado = noempleado;
        }

        public Empleado(string nombre, string apellido)
        {
            Console.WriteLine("Ejecutando el 2do constructor");
            this.nombre = nombre;
            this.apellido = apellido;
            this.noempleado = 0;
        }

        public Empleado()
        {
            Console.WriteLine("Ejecutando el 3er constructor");
            this.nombre = "";
            this.apellido = "";
            this.noempleado = 0;
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public int Noempleado { get => noempleado; set => noempleado = value; }
    }
}
