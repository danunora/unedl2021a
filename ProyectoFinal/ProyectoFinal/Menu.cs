﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoFinal
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void agregarEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Crear una instancia de la ventana de empleados
            Empleados empleados = new Empleados();
            // Asignando el padre de la ventana al menu
            empleados.MdiParent = this;
            // Desplegando la ventana
            empleados.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Crear una instancia de la ventana de empleados
            About about = new About();
            // Asignando el padre de la ventana al menu
            about.MdiParent = this;
            // Desplegando la ventana
            about.Show();

        }
    }
}
