﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        private static void cadenas()
        {
            String cadena = "Esta es una cadena";
            char[] arreglo = cadena.ToCharArray();

            /*
            for (int i = 0; i < arreglo.Length; i++)
            {
                Console.WriteLine(arreglo[i]);
            }
            
            foreach (var caracter in arreglo)
            {
                Console.WriteLine(caracter);
            }
            */

            bool valor = true;
            int i = 0;
            int tamano = arreglo.Length;
            while (valor)
            {
                if (i < tamano)
                {
                    Console.WriteLine(arreglo[i]);
                }
                else
                {
                    valor = false;
                }
                i++;
            }
        }

        private static String MiniMenu()
        {
            Console.Clear();
            Console.WriteLine("Seleccione la opción");
            Console.WriteLine("1 Opción");
            Console.WriteLine("2 Opción");
            Console.WriteLine("0 para salir");
            String valor = Console.ReadLine().Trim();
            return valor;
        }


        private static bool MenuIf(bool respuesta)
        {
            String valor = MiniMenu();
            if (valor == "0")
            {
                respuesta = false;
                return respuesta;
            }
            if (valor == "1")
            {
                cadenas();
            }
            if (valor == "2")
            {
                Console.WriteLine("Ejemplo 2");
            }
            return respuesta;
        }

        private static bool MenuSwitch()
        {
            String valor = MiniMenu();
            bool respuesta = true;
            switch (valor)
            {
                case "1":
                    cadenas();
                    respuesta = true;
                    break;
                case "2":
                    Console.WriteLine("Ejemplo 2");
                    respuesta = true;
                    break;
                case "0":
                    respuesta = false;
                    break;
            }
            return respuesta;
        }

        static void Main(string[] args)
        {
            bool respuesta = true;
            while (respuesta)
            {
                //respuesta = MenuIf(respuesta);

                respuesta = MenuSwitch();
            }
        }
    }
}
