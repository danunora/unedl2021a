package com.oracle;

import java.util.Arrays;
import java.util.Scanner;

class Captura{
	static Scanner scan = new Scanner(System.in);

	static int ingresa(){
		int temp = scan.nextInt();
		return temp;
	}

}

class Arreglos{

	static String matrizLength(int[][] a){
		int[] matriz = new int[a.length];
		for (int i = 0; i < a.length; i++){
			for (int j = 0; j < a[i].length; j++){
				matriz[i] = a[i].length;
			}
		}
		return Arrays.toString(matriz);
	}

	static void matrizDespliega(int[][] a){
		for (int i = 0; i < a.length; i++){
			System.out.println("i: " + i);
			for (int j = 0; j < a[i].length; j++){
				System.out.println("array: " + a[i][j]);
			}
		}
	}
}

public class RecorridoMatriz{

	public static void main(String[] args){
		int m = 0;
		int n = 0;

	  	System.out.println("### Captura la matriz de m x n ###");
	  	System.out.println("Ingresa m: ");
		m = Captura.ingresa();
	  	System.out.println("Ingresa n: ");
		n = Captura.ingresa();

		int[][] a = new int[m][n];

		System.out.println("size: " + Arreglos.matrizLength(a));
		Arreglos.matrizDespliega(a);
	}
}
