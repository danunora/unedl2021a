/*
Given the following matrix: {{1,3,5,7}, {4,7,9,7}, {2,6,8,0}, {2,4,5,2}}

    0 1 2 3
    -------
0 | 1 4 2 2 
1 | 3 7 6 4   =>  N
2 | 5 9 8 5
3 | 7 7 0 2

0,0 = 1
0,1 = 3 

carry out a program that run the matrix so that 
the result is: 1,4,2,2,4,6,7,3,5,9,8,5,2,0,7,7

 i = i + 1;  => i++
*/

public class MyClass {
    public static void main(String args[]) {
        int [][] matriz = {{1,3,5,7,5}, {4,7,9,7,6}, {2,6,8,0,8}, {2,4,5,2,9},{4,5,6,7,8};
        for (int i = 0; i < matriz.length; i++ ){
            for (int j = 0; j < matriz[i].length; j++){
                if ((i % 2) == 0){   
                    System.out.print(" " + matriz[j][i]);
                }                   
                else{
                    System.out.print(" " + matriz[matriz.length - 1 - j][i]);
                }
            }
        }
	    System.out.print("Hello World");
    	System.out.print("My Name is Daniel");
    }
}

// the result is: 1,4,2,2,4,6,7,3,5,9,8,5,2,0,7,7
